class AddAdminUserToStageAssignment < ActiveRecord::Migration
  def change
    add_column :stage_assignments, :admin_user_id, :integer
  end
end
