class AddTimeStampsToStageAssignment < ActiveRecord::Migration
  def change
    add_column :stage_assignments, :time_in, :datetime
    add_column :stage_assignments, :time_out, :datetime
  end
end
