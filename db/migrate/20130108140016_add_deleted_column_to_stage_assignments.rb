class AddDeletedColumnToStageAssignments < ActiveRecord::Migration
  def change
    add_column :stage_assignments, :deleted, :boolean, :default => false
  end
end
