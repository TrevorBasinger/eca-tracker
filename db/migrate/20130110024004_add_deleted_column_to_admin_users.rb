class AddDeletedColumnToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :deleted, :boolean, :default => false
  end
end
