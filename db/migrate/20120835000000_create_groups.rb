class CreateGroups < ActiveRecord::Migration
  def migrate(direction)
    super
    # Create a default user
    Group.create!(:title => 'Administrators') if direction == :up
    Group.create!(:title => 'Management') if direction == :up
    Group.create!(:title => 'Users') if direction == :up
  end
  def change
    create_table :groups do |t|
      t.string :title

      t.timestamps
    end
  end
end
