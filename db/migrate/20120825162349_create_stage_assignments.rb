class CreateStageAssignments < ActiveRecord::Migration
  def change
    create_table :stage_assignments do |t|
      t.integer :stage_id
      t.integer :car_id

      t.timestamps
    end
  end
end
