class AddDeletedColumnToStageTable < ActiveRecord::Migration
  def change
    add_column :stages, :deleted, :boolean, :default => false
  end
end
