EcaTracker::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config

  resources :cars

  namespace 'api' do
    namespace 'v1' do
      resources :stages, :defaults => { :format => 'json' }
      resources :cars, :defaults => { :format => 'json' } do 
        resources :stage_assignments, :defaults => { :format => 'json' }
      end
    end
  end

	match 'stats' => 'stats#index'


  root to: 'cars#index'

end
