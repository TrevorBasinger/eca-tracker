class CarsController < InheritedResources::Base
  respond_to :html, :json

  def index
    if params[:search] then
      @cars = Car.where("code LIKE ? and deleted = ?", "%#{params[:search]}%", false).page params[:page]
    else
      @cars = Car.page params[:page]
    end
  end
end
