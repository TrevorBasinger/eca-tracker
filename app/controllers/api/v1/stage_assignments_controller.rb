module Api
  module V1
    class StageAssignmentsController < ApplicationController
      def index
        @stage_assignments = StageAssignment.find_all_by_car_id(params[:car_id])
      end

      def show
        @stage_assignment = StageAssignment.find_by_id(params[:id])
      end

      def edit
        @stage_assignment = StageAssignment.find_by_id(params[:id])

        respond_to do | format |
          if @stage_assignment.update_attributes(params[:stage_assignment])
            format.json { head :no_content }
          else
            format.json { render json: @stage_assignment.errors, status: :unprocessable_entity }
          end
        end
      end

      def update
        @stage_assignment = StageAssignment.find(params[:id])
        respond_to do | format |
          if @stage_assignment.update_attributes(params[:stage_assignment])
            format.json { render json: @stage_assignment }
          else
            format.json { render json: @stage_assignment.errors, status: :unprocessable_entity }
          end
        end
      end

      def create
        @stage_assignment = StageAssignment.new(params[:stage_assignment])
        respond_to do | format |
          if(@stage_assignment.save)
            format.json { render json: @stage_assignment }
          else
            format.json { render json: @stage_assignment.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end
end
