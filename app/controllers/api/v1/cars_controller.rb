module Api
  module V1
    class CarsController < ApplicationController
      def index
        if(params[:code])
          @cars = Car.find_all_by_code(params[:code])
        else
          @cars = Car.where(:deleted => false);
        end
      end

      def show
        @car = Car.find_by_id(params[:id])
      end

      def update
        @car = Car.find(params[:id])
        respond_to do | format |
          if @car.update_attributes(params[:car])
            format.json { render json: @car }
          else
            format.json { render json: @car.errors, status: :unprocessable_entity }
          end
        end
      end

      def create
        @car = Car.new(params[:car])
        respond_to do | format |
          if(@car.save)
            format.json { render json: @car}
          else
            format.json { render json: @car.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end
end
