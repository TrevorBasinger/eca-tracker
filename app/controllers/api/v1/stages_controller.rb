module Api
  module V1
    class StagesController < ApplicationController
      ##
      # Takes 'title' as parameter in url
      #
      def index
        if(params[:title])
          #@stages = Stage.find_all_by_title(params[:title])
          @stages = Stage.find(:first, :conditions => ['lower(title) = ?', params[:title].downcase])
        else
          @stages = Stage.where(:deleted => false)
        end
        respond_to do |format|
          format.json { render json: @stages }
        end
      end

      def show
        @stage = Stage.find(params[:id])

        respond_to do | format |
          format.json { render json: @stage }
        end
      end

    end
  end
end
