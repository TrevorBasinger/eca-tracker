class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :group_id, :group
  # attr_accessible :title, :body
  belongs_to :group

  validates_presence_of :group

  def model_name
    'User'
  end
	
	def destroy
		self.deleted = true;
		self.save
	end

	def delete
		self.destroy
	end
end
