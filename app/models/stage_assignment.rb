class StageAssignment < ActiveRecord::Base
  attr_accessible :car_id, :stage_id, :time_in, :time_out, :admin_user_id
  # Add validation to prevent multiple stage assignments from being open
  # this particular validation prevents a stage assignment from being updated.
  #validate :has_no_active_stage_assignments_for_car
  belongs_to :car
  belongs_to :stage
  belongs_to :admin_user

  validates_presence_of :stage_id, :car_id

  def is_open?
    self.time_in and !self.time_out
  end

  def has_no_active_stage_assignments_for_car
    if self.time_out == nil && self.car.has_open_stage_assignments?
      self.errors.add(:car, "car already has open stage assignments") 
    end
  end

  def last_modified_by
    self.admin_user.email
  end

end
