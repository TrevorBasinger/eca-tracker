class Group < ActiveRecord::Base
  attr_accessible :title
  validates_presence_of :title
  has_many :admin_users
end
