class Stage < ActiveRecord::Base
  attr_accessible :title, :deleted

  has_many :stage_assignments
  has_many :cars, through: :stage_assignments

  validates_presence_of :title
	
	#default_scope where(:deleted => false);

	def destroy
		self.deleted = true;
		self.save
	end

	def delete
		self.destroy
	end

end
