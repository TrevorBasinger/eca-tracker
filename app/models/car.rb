class Car < ActiveRecord::Base
  attr_accessible :code, :make, :model, :deleted

  has_many :stage_assignments, :dependent => :destroy
  has_many :stages, :through => :stage_assignments, :dependent => :destroy

	# Active admin needs this commented out
	#default_scope where(:deleted => false);

  validates_presence_of :code, :make, :model

	validates_uniqueness_of :code

  def title
    self.code + ' - ' + self.make + ' ' + self.model
  end

  def has_open_stage_assignments?
    self.stage_assignments.map { | sa |
      sa.is_open?
    }.include?(true)
  end

  def active_stages
    self.stage_assignments.select { | sa |
      sa.stage if sa.is_open?
    }.map { | sa |
      sa.stage
    }
  end

  def active_stage
    self.active_stages.first
  end

	def active_stage_title
		stage = self.active_stage
		if stage then
			return stage.title
		end
		''
	end

	def active_assignment
		self.stage_assignments.select { |sa|
			sa if sa.is_open?
		}.last
	end

	def active_assignments
		self.stage_assignments.select { |sa|
			sa if sa.is_open?
		}
	end

	def destroy
		self.deleted = true;
		self.save
	end

	def delete
		self.destroy
	end

	def days_in_stage
		sa = self.stage_assignments.last
		return 0 unless sa != nil

		return (Date.today - Date.parse(sa.time_in.to_s)).to_i unless sa.time_out != nil
		return (Date.parse(sa.time_out.to_s) - DateTime.parse(sa.time_in.to_s)).to_i
	end
end
