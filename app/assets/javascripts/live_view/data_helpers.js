(function() {

  $(function() {
    var DataHelpers, _ref;
    DataHelpers = (function() {

      function DataHelpers() {}

      DataHelpers.prototype.sort_cars_by_code = function(cars) {
        if (!cars) {
          throw 'InvalidArgumentException';
        }
        if (!cars instanceof Array) {
          throw 'InvalidArgumentException';
        }
        return this.sortall_by_attribute(cars, 'code');
      };

      DataHelpers.prototype.sort = function(a, b) {
        if (a >= b) {
          return true;
        } else {
          return false;
        }
      };

      DataHelpers.prototype.sortall = function(array) {
        var a, b, current, final, _i, _j, _len, _len1;
        final = [];
        for (_i = 0, _len = array.length; _i < _len; _i++) {
          a = array[_i];
          current = a;
          for (_j = 0, _len1 = array.length; _j < _len1; _j++) {
            b = array[_j];
            if (this.sort(a, current)) {
              current = b;
            }
          }
          final.push(current);
        }
        return final;
      };

      DataHelpers.prototype.sortall_by_attribute = function(array, attr) {
        var a, b, current, final, _i, _j, _len, _len1;
        final = [];
        for (_i = 0, _len = array.length; _i < _len; _i++) {
          a = array[_i];
          current = a;
          for (_j = 0, _len1 = array.length; _j < _len1; _j++) {
            b = array[_j];
            if (this.sort(a[attr], current[attr])) {
              current = b;
            }
          }
          final.push(current);
        }
        return final;
      };

      return DataHelpers;

    })();
    return (_ref = window.DataHelpers) != null ? _ref : window.DataHelpers = DataHelpers;
  });

}).call(this);
