$ ->
	class DataHelpers
		sort_cars_by_code: (cars) ->
			throw 'InvalidArgumentException' if not cars
			throw 'InvalidArgumentException' if not cars instanceof Array
			@sortall_by_attribute(cars, 'code')

		sort: (a, b) ->
			if	a >= b then true else false

		sortall: (array) ->
			final = []
			for a in array
				current = a
				for b in array
					current = b if @sort a, current
				final.push current
			final

		sortall_by_attribute: (array, attr) ->
			final = []
			for a in array
				current = a
				for b in array
					current = b if @sort a[attr], current[attr]
				final.push current
			final

	window.DataHelpers ?= DataHelpers
