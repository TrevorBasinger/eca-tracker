$ ->
	class LiveViewHelper

		@get_marquee_text: (cars) ->
			return "" if cars is undefined or null
			text = ""
			stages = @get_unique_stages cars
			for stage in stages
				count = 0
				for car in cars
					count++ if car.get('active_stage_title') == stage
				text = text + "#{stage} has #{count} -- "

			text

		@get_unique_stages: (cars) ->
			return [] if cars is undefined or null
			stages = []
			for car in cars
				stages.push(car.get('active_stage_title')) 
			stages = _.uniq(stages)

	window.LiveViewHelper = LiveViewHelper
