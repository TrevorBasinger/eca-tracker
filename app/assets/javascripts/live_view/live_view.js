(function() {

  $(function() {
    var LiveViewHelper;
    LiveViewHelper = (function() {

      function LiveViewHelper() {}

      LiveViewHelper.get_marquee_text = function(cars) {
        var car, count, stage, stages, text, _i, _j, _len, _len1;
        if (cars === void 0 || null) {
          return "";
        }
        text = "";
        stages = this.get_unique_stages(cars);
        for (_i = 0, _len = stages.length; _i < _len; _i++) {
          stage = stages[_i];
          count = 0;
          for (_j = 0, _len1 = cars.length; _j < _len1; _j++) {
            car = cars[_j];
            if (car.get('active_stage_title') === stage) {
              count++;
            }
          }
          text = text + ("" + stage + " has " + count + " -- ");
        }
        return text;
      };

      LiveViewHelper.get_unique_stages = function(cars) {
        var car, stages, _i, _len;
        if (cars === void 0 || null) {
          return [];
        }
        stages = [];
        for (_i = 0, _len = cars.length; _i < _len; _i++) {
          car = cars[_i];
          stages.push(car.get('active_stage_title'));
        }
        return stages = _.uniq(stages);
      };

      return LiveViewHelper;

    })();
    return window.LiveViewHelper = LiveViewHelper;
  });

}).call(this);
