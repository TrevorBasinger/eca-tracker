/**
 *
 * Requires Date.js
 * Requires JST.js
 * Requires Backbone.js
 *    -> and Underscore.js
 *
 */

Backbone.View.prototype.close = function() {
  this.remove();
  this.unbind();
}

window.EcaTrackerApp = { };

EcaTrackerApp.AppView = {
  showView: function(view) {
    if(this.currentView) {
      this.currentView.close();
    }
    this.currentView = view;
    this.currentView.render();

    $('div#appview').html(this.currentView.el);
  }
};

EcaTrackerApp.Router = Backbone.Router.extend({
  initialize: function(options) {
    this.options = options;
    this.appView = EcaTrackerApp.AppView;
    this.cars
  },
  routes: {
    '': 'main_dash',
    ':id': 'main_dash'
  },
  options: null,
  main_dash: function(id) {
    var carCollection = new EcaTrackerApp.CarCollection();
    carCollection.reset(this.options.car_collection);
    this.cars = carCollection;
    carCollection.code = id;
    // For debugging models
    this.options.cars = carCollection;
    var dcView = new EcaTrackerApp.DashCarWidgetView({collection: carCollection , stages: this.options.stages });
    this.appView.showView(dcView);
  }
});

/**
 * Stage
 */
EcaTrackerApp.StageModel = Backbone.Model.extend({
});

EcaTrackerApp.StageCollection = Backbone.Collection.extend({
  model: EcaTrackerApp.StageModel,
  url: function() { return '/api/v1/stages/'; },
});

/**
 * Stage Assignment
 */

// Requires a car_id as attribute
EcaTrackerApp.StageAssignmentModel = Backbone.Model.extend({
  initialize: function() {
    this.stage = new EcaTrackerApp.StageModel(this.get('stage'));
    this.stage.url = '/api/v1/stages/'+ this.get('stage_id');
  },
  url: function() {
    return '/api/v1/cars/'+ this.get('car_id') + 
           '/stage_assignments/' + 
           ( this.get('id') ? this.get('id') : '');
  },
  isClockedIn: function() {
    if(this.get('time_in') && this.get('time_out')==null) return true;
    return false;
  },
});

EcaTrackerApp.StageAssignmentRowView = Backbone.View.extend({
  initialize: function() {
    this.model.bind('change', this.render, this); 
  },
  events: {
    "click .clock_out" : "clockOut"
  },
  tagName: 'tr',
  template: JST['active_cars/show'],
  render: function() {
    if(this.model)
      this.$el.html(this.template(this.model.toJSON()));
    return this;
  },

  clockOut: function(a){ this.model.clockOut() }

});

EcaTrackerApp.StageAssignmentCollection = Backbone.Collection.extend({
  model: EcaTrackerApp.StageAssignmentModel,
  areClockedIn: function() {
    return this.filter(function(sa) { return sa.isClockedIn() }, this).length > 0;
  },
  comparator: function(sa) {
    return sa.get('id');
  }
});

/**
 * Car
 */
 
EcaTrackerApp.CarModel = Backbone.Model.extend({
  initialize: function() {
    this.stage_assignments = new EcaTrackerApp.StageAssignmentCollection();
    this.stage_assignments.url = '/api/v1/cars/' + this.id + '/stage_assignments';
    this.stage_assignments.reset(this.get('stage_assignments'));
  },

  stage_assignments : null,

  url: function(){ return '/api/v1/cars/'+this.get('id')},

  clockIn: function(stage) {
    var stage_assignment = new EcaTrackerApp.StageAssignmentModel();
    stage_assignment.stage = stage;
    var slf = this;
    stage_assignment.save({ 
      car_id: this.get('id'),
      stage_id: stage.get('id'),
      time_in: Date.now().toString("yyyy-MM-ddTHH:mm:ssZ"),
      time_out: null,
      admin_user_id: window.router.options.uid
    }, 
    { 
      success: function(model, response) { slf.stage_assignments.push(model); },
      error: function(model, response) { /* Do nothing */ }
    });
  },

  clockOut: function() {
    var slf = this;
    var clocked_in = this.stage_assignments.select(function(sa){
      return (sa.get('time_out') == null || sa.get('time_out') == undefined);
    }, this);

    _(clocked_in).each( function(sa) {
        sa.save(
          {time_out: Date.now().toString("yyyy-MM-ddTHH:mm:ssZ"),
            admin_user_id: window.router.options.uid },
          {
            success: function(model, response) {
              slf.update();
            }
          }
        );
    }, this);
  },
  update: function() {
    this.trigger('change');
  }
});

EcaTrackerApp.CarCollection = Backbone.Collection.extend({
  initialize: function() { },
  model: EcaTrackerApp.CarModel,
  url: function() { return '/api/v1/cars'+((this.code != null) ? '?code='+this.code : ''); },
  code: null 
});


/**
 * Dash
 */
EcaTrackerApp.DashCarWidgetRowView = Backbone.View.extend({
  initialize: function() {
    this.model.bind('change', this.render, this);
    this.model.bind('update', this.render, this);
    this.model.stage_assignments.bind('add', this.render, this);
  },
  events: { 
    'click #clock_out': function(){ this.model.clockOut(); },
    'click #clock_in' : function(){ 
        var stage = this.options.stages.select( function(s){
          return s.get('id') == this.$el.find('select option:selected').val();
        }, this);
        if(stage.length > 0) this.model.clockIn(stage[0]);
      },
  },
  template: JST['widgets/dashcarwidgetrow'],
  tagName: 'tr',
  render: function() {
    // Build template with select element and options
    var previousStage = this.getPreviousStage();
    this.$el.html(this.template({car: this.model.toJSON(), select_widget: this.getStageSelectWidget(), previous_stage: previousStage})); 

      
    // If clocked in disable select element and hide/show appropriate buttons
    if(this.isClockedIn()) {
      this.$el.find('td select').attr('disabled', true);
      this.$el.find('#clock_out').css('display','inline-block').show();
      this.$el.find('#clock_in').hide();
      
      // Get the stage name and select the appropriate option
      if(this.model.stage_assignments.last() != undefined) {
        if(this.model.stage_assignments.last().stage.get('id') != undefined) {
          var id = this.model.stage_assignments.last().stage.get('id'); 
          var opts = _.select(this.$el.find('option'), function(o) { return $(o).val() == id }, this);
          $(opts[0]).attr('selected','selected');
        }
      }
    }
    // If clocked out enable select element and hide/show appropriate buttons
    else {
      this.$el.find('td select').attr('disabled', false);
      this.$el.find('#clock_out').hide();
      this.$el.find('#clock_in').css('display','inline-block').show(); 
    }
    return this;
  },

  getStageSelectWidget: function() {
    var select = "<select>"
    this.options.stages.forEach(function(s){ 
      select=select+'<option value="'+s.get('id')+'">'+s.get('title')+'</option>'
    }, this);
    return select+'</select>'
  },

  getPreviousStage: function() {
    var closed = this.model.stage_assignments.select(function(sa){
      return (sa.get('time_out') != null);
    });
    var last = closed[closed.length -1];
    if(last == undefined || last == null) return '';
    return (last.stage.get('title') ? last.stage.get('title') : '');
  },

  isClockedIn: function() {
    if(this.model.stage_assignments.length == 0) return false;
    return this.model.stage_assignments.areClockedIn();
  }
});

// Requires a backbone.collection of models
// Requires an array of 'stages' for select option elements
EcaTrackerApp.DashCarWidgetView = Backbone.View.extend({
  template: JST['widgets/dashcarwidget'],            

  events : {
    'click input#filtersubmit': function() { window.document.location = '/admin?search='+ this.$el.find('input#codefilter').val() }
  },

  render: function() {
    if(this.collection == null) return this;
    if(this.options.stages == null) return this;

    this.$el.html(this.template);
    this.collection.forEach(function(car) { 
      this.$el.find('tbody').append(new EcaTrackerApp.DashCarWidgetRowView({model: car, stages: this.options.stages}).render().el);
    }, this);
  }
});
