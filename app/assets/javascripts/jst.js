window.JST = {};
window.JST['active_cars/show'] = _.template(
    '<td><%= car.code %></td><td><%= car.make %></td><td><%= car.model %></td><td><%=stage.title%></td><td><a class="clock_out">Clock Out</a> <a class="clock_in">Clock In</a> <a class="complete">Complete</a></td>'
);

window.JST['widgets/dashcarwidgetrow'] = _.template(
'<td><a href="../../../admin/stage_assignments?q[car_code_contains]=<%= car.code %>"><%= car.code %></a></td><td><%= car.make %></td><td><%= car.model %></td><td class="stage"><%= select_widget %><a id="clock_in" class="stage">Clock In</a><a id="clock_out" class="stage">Clock Out</a></td>'+
'<td><%= previous_stage %></td>'
);

window.JST['widgets/dashcarwidget'] = _.template(
  'Code: <input id="codefilter" /><input id="filtersubmit" type="submit"/>'+
  '<table class="dash">'+
    '<thead>'+
      '<th>Code</th>'+
      '<th>Make</th>'+
      '<th>Model</th>'+
      '<th>Stage</th>'+
      '<th>Previous Stage</th>'+
    '</thead>'+
    '<tbody></tbody>'+
  '</table>');
