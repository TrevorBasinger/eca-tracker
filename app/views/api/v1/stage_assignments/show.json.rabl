object @stage_assignment
attributes :id, :time_in, :time_out
child :stage do
  extends 'api/v1/stages/show'
end
