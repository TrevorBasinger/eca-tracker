object @car
attributes :id, :code, :make, :model, :created_at, :days_in_stage, :active_stage_title
child :stage_assignments do
  extends 'api/v1/stage_assignments/index'
end
