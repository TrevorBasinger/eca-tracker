ActiveAdmin.register Car do
  menu :if => proc{ can?(:manage, Car) }

	controller do
		def scoped_collection
			Car.where(:deleted => false)
		end
	end

  index do
    column 'Number of Stages' do | car |
      car.stage_assignments.length
    end
    column :code, :sortable => :code do | car |
      link_to car.code.upcase, admin_car_path(car)
    end
    column :make, :sortable => :make
    column :model, :sortable => :model
    column :created_at, :sortable => :created_at
    column :actions do | car |
      link_to 'edit', edit_admin_car_path(car)
    end
  end

end
