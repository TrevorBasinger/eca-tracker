ActiveAdmin.register AdminUser  do     
  menu :if => proc{ can?(:manage, AdminUser) }, :label => "Users"
  
  class AdminUser
    def title
      self.email
    end
  end

	controller do
		def scoped_collection
			AdminUser.where(:deleted => false)
		end
	end

  
  index :title => 'Users' do                            
    column :email                     
    column :current_sign_in_at        
    column :last_sign_in_at           
    column :sign_in_count             
    default_actions                   
  end                                 

  filter :email                       

  form do |f|                         
    f.inputs "Admin Details" do       
      f.input :email                  
      f.input :password               
      f.input :password_confirmation  
      f.input :group
    end                               
    f.buttons                         
  end                                 
end                                   
