ActiveAdmin.register StageAssignment do
  menu :if => proc{ can?(:manage, StageAssignment) }     

  controller do
    def scoped_collection
      end_of_association_chain.includes(:car, :stage, :admin_user)
    end
  end

  filter :car_code, :as => :string
  filter :car_make, :as => :string
  filter :car_model, :as => :string
  filter :stage
  filter :admin_user
  filter :time_in
  filter :time_out
    
  batch_action :destroy , :confirm => "Are you sure you want to delete these stage assignments" do | selection |
    StageAssignment.find(selection).each do | sa |
      (sa.destroy; sa.save) if current_admin_user.group == Group.find_by_title('administrator')  
    end
    redirect_to :controller => :stage_assignments
  end

  index do
    selectable_column if current_admin_user.group == Group.find_by_title('administrator')

    column "Code", :code, :sortable => :'cars.code'  do | sa |
      link_to sa.car.code.upcase, :controller => 'stage_assignments', :action => 'index', 'q[car_code_contains]' => sa.car.code.html_safe
    end

    column "Make", :make, :sortable => :'cars.make' do | sa |
      sa.car.make.upcase
    end

    column "Model", :model, :sortable => :'cars.model' do | sa |
      sa.car.model.upcase
    end

    column "Stage", :sortable => :'stages.title' do | sa |
      sa.stage.title.upcase
    end

    column "Last Modified By", :admin_user, :sortable => :'admin_users.email' do | sa |
      sa.last_modified_by
    end

		column "Car deleted", :sortable => 'cars.deleted' do | sa |
			sa.car.deleted
		end
    
    column :time_in
    column :time_out

    default_actions if current_admin_user.group == Group.find_by_title('administrator')

  end
  
end
