ActiveAdmin.register Stage do
  menu :if => proc{ can?(:manage, Stage) }     

	controller do
		def scoped_collection
			Stage.where(:deleted => false)
		end
	end


  index do
    column 'Title', :sortable => :title do | stage |
      stage.title
    end
    column :created_at, :sortable => :created_at
    column :actions do | stage |
      link_to 'edit', edit_admin_stage_path(stage)
    end
	end
end
